import functools


class MyException1(Exception):
    pass


class MyException2(Exception):
    pass


class MyException3(Exception):
    pass


def my_flags_worker(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        try:
            res = func(*args, **kwargs)
        except MyException1:
            print("ex 1")
        except MyException2:
            print("ex 2")
        except MyException3:
            print("ex 3")
        return
    return wrapper


@my_flags_worker
def func_1():
    while True:
        a = input("----->")
        if a.isdigit():
            raise MyException1
        elif a.isalpha():
            raise MyException2
        else:
            raise MyException3

if __name__ == "__main__":
    func_1()

