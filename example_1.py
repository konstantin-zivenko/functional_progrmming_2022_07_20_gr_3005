from decorators import do_twice, do_do_twice


@do_twice
def say_whee():
    """It is my function."""
    print("Whee!!!")


@do_twice
def greet(name):
    print(f"Hello {name}")

@do_do_twice
def return_greeting(name):
    print("Creating greeting")
    return f"Hi {name}"

say_whee()

greet("Mary")

hi_adam = return_greeting("Adam")

print(hi_adam)